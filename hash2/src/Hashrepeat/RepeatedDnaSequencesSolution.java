package Hashrepeat;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class RepeatedDnaSequencesSolution 
{
    private static final HashMap<Character, Integer> encodings = new HashMap<>();
    static { encodings.put('A',0); encodings.put('C',1); encodings.put('G',2); encodings.put('T',3); }
    private final int Two_POW_9 = (int) Math.pow(2, 9);
 
    public ArrayList<String> findRepeatedDnaSequences(String s) {
 
        Set<String>       res = new HashSet<>();
        //Set<Integer>   hashes = new HashSet<>();
      // List<String> solution = new ArrayList<String>();        
         
        HashMap<Integer, String> duplicates = new HashMap<Integer, String>();
         
        for (int i = 0, rhash = 0; i < s.length(); i++) 
        {
            if (i > 9) rhash -= Two_POW_9 * encodings.get(s.charAt(i-10));
             
            rhash = 2 * rhash + encodings.get(s.charAt(i));
             
            if (i > 8) 
            {
                if (duplicates.get(rhash) != null)
                {
                    res.add(s.substring(i-9,i+1));
                }
                else
                {
                    duplicates.put(rhash, "");
                }
            }
        }
        return new ArrayList<String>(res);
    }   
}